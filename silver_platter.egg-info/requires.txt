breezy>=3.3.1
dulwich>=0.20.23
jinja2
pyyaml

[debian]
brz-debian
debmutate>=0.3
python_debian>=0.1.48

[detect-gbp-dch]
lintian-brush

[launchpad]
launchpadlib
