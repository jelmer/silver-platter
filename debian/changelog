silver-platter (0.5.6-2) unstable; urgency=medium

  * Fix test suite invocation in debian/tests/control. Closes: #1031303

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Feb 2023 19:42:23 +0000

silver-platter (0.5.6-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 05 Feb 2023 13:46:56 +0000

silver-platter (0.5.3-1) unstable; urgency=medium

  [ Debian Janitor ]
  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 16 Dec 2022 16:27:19 +0000

silver-platter (0.5.0-1) unstable; urgency=medium

  * debian/upstream/metadata: Update Repository location.
  * New upstream release.
   + Fixes compatibility with newer Breezy. Closes: #1021796

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 23 Oct 2022 10:26:54 +0100

silver-platter (0.4.5-1) unstable; urgency=low

  * New upstream release.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 01 Jan 2022 14:27:00 +0000

silver-platter (0.4.3-1) unstable; urgency=low

  * New upstream release.
   + Add dependency on python3-jinja2.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 08 Jul 2021 09:25:37 +0100

silver-platter (0.4.1-2) unstable; urgency=medium

  * Bump minimum breezy-debian version to something that supports
    debcommit. Closes: #983008

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 18 Feb 2021 13:05:11 +0000

silver-platter (0.4.1-1) unstable; urgency=medium

  * New upstream release.
   + Depend on upstream ontologist for upstream metadata. Closes: #982994

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 17 Feb 2021 22:47:59 +0000

silver-platter (0.4.0-1) unstable; urgency=medium

  * New upstream release.
   + Add subcommand to upload pending changes. Closes: #942602
   + Add subcommand import-upload to upload changes missing from
     VCS. Closes: #942598
  * Re-export upstream signing key without extra signatures.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 01 Feb 2021 23:34:24 +0000

silver-platter (0.3.0+git20201123.02992d4-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 25 Nov 2020 23:12:18 +0000

silver-platter (0.3.0+git20200906.d2bd137-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 06 Sep 2020 19:58:55 +0000

silver-platter (0.3.0+git20200906.07a6d1b-1) unstable; urgency=medium

  * New upstream snapshot.
  * Fixes compatibility with newer versions of breezy-debian. Closes:
    #969665
  * New upstream snapshot.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 06 Sep 2020 19:48:41 +0000

silver-platter (0.3.0+git20200611.b4292bf-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 11 Jun 2020 01:54:55 +0000

silver-platter (0.3.0+git20200530.3e19145-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 31 May 2020 02:03:44 +0000

silver-platter (0.3.0+git20200517.38c492c-1) unstable; urgency=medium

  * New upstream snapshot.
   + Ensures breezy is correctly initialized. Closes: #960862

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 17 May 2020 18:02:49 +0000

silver-platter (0.3.0+git20200516.9f9af15-1) unstable; urgency=medium

  * New upstream snapshot.
   + Fixes compatibility with newer brz-debian. Closes: #960751
  * Bump debhelper from old 12 to 13.
  * New upstream snapshot.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 16 May 2020 14:10:10 +0000

silver-platter (0.3.0-1) unstable; urgency=medium

  * New upstream snapshot.
  * Set Rules-Requires-Root.
  * New upstream release.
  * Update standards version to 4.5.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Apr 2020 23:57:52 +0000

silver-platter (0.2.0+git20191022.7591492-1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * New upstream snapshot.
  * Update standards version to 4.4.1, no changes needed.
  * Re-export upstream signing key without extra signatures.
  * Remove unnecessary build-dependency on python-distro-info. Closes:
    #943211

  [ Debian Janitor ]
  * Update standards version to 4.4.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 23 Oct 2019 22:36:19 +0000

silver-platter (0.2.0+git20190901.fe90a04-1) unstable; urgency=medium

  * Bump dependency on brz-debian to 2.8.28.
  * debian/tests/control: Add missing dependency on python3-
    breezy.tests.
  * New upstream snapshot.
  * Bump minimum brz-debian to 2.8.30.
  * Update standards version, no changes needed.
  * Bump debhelper from old 11 to 12.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * Bump minimum lintian-brush to 0.28.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 05 Sep 2019 01:46:23 +0000

silver-platter (0.1.0-1) unstable; urgency=medium

  * debian/copyright: Fix copyright year.
  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 06 Apr 2019 23:25:28 +0000

silver-platter (0.0.3-1) experimental; urgency=medium

  * Initial release. (Closes: #921302)

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 10 Feb 2019 17:11:43 +0000
