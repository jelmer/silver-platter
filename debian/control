Source: silver-platter
Section: vcs
Priority: optional
Maintainer: Jelmer Vernooĳ <jelmer@debian.org>
Build-Depends: dh-python,
               brz-debian (>= 2.8.41),
               python3-all,
               python3-breezy (>= 3.3.0),
               python3-breezy.tests,
               python3-distro-info,
               python3-dulwich (>= 0.19.7),
               python3-launchpadlib,
               python3-jinja2,
               python3-pip,
               python3-yaml,
               python3-testtools,
               python3-upstream-ontologist,
               lintian-brush (>= 0.70),
               debhelper-compat (= 13)
Standards-Version: 4.6.0
Homepage: https://www.jelmer.uk/code/silver-platter
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/jelmer/silver-platter.git
Vcs-Browser: https://salsa.debian.org/jelmer/silver-platter

Package: silver-platter
Architecture: all
Depends: devscripts,
         brz-debian (>= 2.8.50),
         python3-breezy (>= 3.3.0),
         python3-dulwich (>= 0.19.7),
         python3-distro-info,
         python3-jinja2,
         python3-launchpadlib,
         python3-yaml,
         python3-upstream-ontologist,
         ${misc:Depends},
         ${python3:Depends}
Conflicts: lintian-brush (<< 0.40)
Recommends: brz, lintian-brush (>= 0.70)
Description: automatically create merge proposals
 Silver-Platter makes it possible to contribute automatable changes to source
 code in a version control system.
 .
 It automatically creates a local checkout of a remote repository,
 make user-specified changes, publish those changes on the remote hosting
 site and then creates pull request.
 .
 In addition to that, it can also perform basic maintenance on branches
 that have been proposed for merging - such as restarting them if they
 have conflicts due to upstream changes.
